 import java.util.Scanner;
 public class toaster {
	Scanner sc=new Scanner(System.in);
	 private double size;
	 private int toasts;
	 private String colour;
	 
	 public void printSize() {
		 System.out.println("this is the size of your toaster: " + this.size);
	 }
	 
	 public void cooksBread() {
		 System.out.println("your toaster can make " + this.toasts + " toasts at once.");
	 }
	 
	 public int validator(int size) {
		 while (size<=0) {
			 System.out.println("Enter another number");
		 size=sc.nextInt();
		 }	 
		 return size;
	 }
	 
	 public void printSizeX2(int size) {
		this.size=validator(size);
		System.out.println("the size of two of your toaster is " + (this.size*2));
	 }
	 
	 public double getSize() {
		 return this.size;
	 }
	 
	 public void setSize(double newSize) {
		 this.size=newSize;
	 }
	 
	 public int getToasts() {
		 return this.toasts;
	 }
	 
	 public void setToasts(int newToasts) {
		 this.toasts=newToasts;
	 }
	 
	 public String getColour() {
		 return this.colour;
	 }
	 
	 public void setColour(String newColour) {
		this.colour=newColour;
 }
	
	public void allFields(double size, int toasts, String colour) {
		this.size=size;
		this.toasts=toasts;
		this.colour=colour;
 }
 }
	 